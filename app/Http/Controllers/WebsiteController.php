<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
class WebsiteController extends Controller
{
  public function index()
  {
    return view('index');
  }
  public function about()
  {
    return view('about-us');
  }

  public function our_offering()
  {
    return view('our-offerings');
  }

  public function team()
  {
    return view('team');
  }

  public function contact_us()
  {
    return view('contact-us');
  }

  public function net_practice()
  {
    return view('net-practice');
  }

  public function matches()
  {
    return view('matches');
  }

  public function send_email(Request $request)
  {
    $test ="test";
    Mail::send('emails', ['first_name' => $request->name,'email'=>$request->email,'phone_number'=>$request->phone_number,'query' => $request->message], function ($message) use ($request)
    {
      $message->subject("Contact Us Form");
      $message->to('profdeepak0101@gmail.com');
      $message->from('dahisarbluescc167@gmail.com');
    });
    //dd($test);
    return redirect()->route('contact-us')->with('message','We will get back to you within 24 hours!');
  }

  public function gallery()
  {
    return view('gallery');
  }
}
