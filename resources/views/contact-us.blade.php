@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row mx-auto">
    <div class ="col-lg-2">
    </div>
    <div class ="col-lg-10">
      <div class="card">
        <div class="card-header">
          Contact Us
          @if(Session::has('message'))
          <p class="alert alert-success float-right">{{ Session::get('message') }}</p>
          @endif
        </div>
        <div class="card-body">
          <div class="form">
            {!! Form::open(['method' => 'POST', 'route' => ['send-email'] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>Name</h6>
              {!! Form::text('name', old('name'), ['class' => 'form-control ', 'placeholder' => '']) !!}
              @if($errors->has('name'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('name') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Email ID</h6>
              {!! Form::email('email', old('email'), ['class' => 'form-control ', 'placeholder' => '']) !!}
              @if($errors->has('email'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('email') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Phone Number</h6>
              {!! Form::tel('phone_number', old('phone_number'), ['class' => 'form-control ', 'placeholder' => '']) !!}
              @if($errors->has('phone_number'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('phone_number') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>Your Message</h6>
              {!! Form::text('message', old('message'), ['class' => 'form-control ', 'placeholder' => '']) !!}
            </div>
            <br>
            {!! Form::submit("Send Message", ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<br>
@endsection
