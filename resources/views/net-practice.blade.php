@extends('layouts.app')
@section('content')
<section>
  <div class="container">
    <h2>Our Practice Drills</h2>
    <br><br>
    <div class="row">
      <div class="col-md-12 col-xl-12">
        <h2>Batting Drills</h2>
        <div class="row text-center text-lg-left">
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/batting-drill-1.jpg')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/batting-drill-2.jpg')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/batting-drill-3.jpg')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/batting-drill-4.jpg')}}" alt="">
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-12">
        <h2>Bowling Drills</h2>
        <div class="row text-center text-lg-left">
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/bowling-drill-1.jpeg')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/bowling-drill-2.jpeg')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/bowling-drill-3.jpeg')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/bowling-drill-4.jpeg')}}" alt="">
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-12">
        <h2>Fielding Drills</h2>
        <div class="row text-center text-lg-left">
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/fielding-drills-1.png')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/fielding-drills-2.png')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/fielding-drills-3.png')}}" alt="">
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ url('img/practice/fielding-drills-4.jpg')}}" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('partials.social-media')
@include('partials.javascripts')
@stop
