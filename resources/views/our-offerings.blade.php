@extends('layouts.app')
@section('content')
<section>
  <div class="container">
    <h2>Our Offerings</h2>
    <br><br>

    <div class="row">
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <h4>Our Services
          </h4>
          <ol type = 1>
            <li>Cricket Coaching Around The Year</li>
            <li>Friendly Cricket Matches</li>
            <li>Pvt Tournaments</li>
            <li>Corporate Tournaments</li>
            <li>Mca Tournaments From Some Other Club</li>
            <li>MCA CARD</li>
            <li>Cricket Tours (National)</li>
            <li>Cricket Tours (International)</li>
            <li>Cricket Coaching Clinic (At Our Club)</li>
            <li>Cricket Coaching Consutancy (Online)</li>
          </ol>
        </div>
      </div>
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <h4>Cricket Coaching Clinic</h4>
          <p>Cricket Coaching Clinic Facility Is Available – Any Player From Our Club Or From Any Other Club Anywhere From India Or Abroad Can Get Free Advise With Respect To Any Problems In His /her Batting , Bowling , Fielding Or Mental Toughness To Improve His /her Performance. A Proper Plan , Exercise & Diet Will Be Given To Player With 100% Result Orienatation.
          </p>
        </div>
      </div>
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <h4>Cricket Coaching Consultancy (Online)</h4>
          <p>Players / Parents From Any Part Of India Can Contact Us For Advise / Suggestion / Recommendations With Respect To Cricket Coaching For His /her Child. We Also Invite Cricket Coaches From All Over India To Register With Us For Better Opportunities And For Learning / Providing The Best Quality Cricket Coaching.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<br><br>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xl-6">
        <div class="about_text">
          <h4>What is New?</h4>
          <ol type = 1>
            <li>Professional Level Coaching </li>
            <li>Group Of 10 - 15 Players</li>
            <li>Personal Coaching One To One Ratio</li>
            <li>Indoor Cricket Coaching</li>
            <li>Exercise & Fitness</li>
            <li>Batting Drills To Develop Various Skills Batting</li>
            <li>Fielding Drill – To Develop Various Skills Of Fielding And Do Develop Agility In Players </li>
            <li>Bowling Drills Different Drills For Fast Bowlers And Spinners </li>
            <li>Tips From Experts</li>
            <li>Video Analysis</li>
            <li>Real Match Practice In Open Ground </li>
            <li>Group Discussion</li>
            <li>Development Of Match Temperament</li>
          </ol>
        </div>
      </div>
      <div class="col-md-12 col-xl-6">
        <div class="about_text">
          <h4>Personal Level Coaching</h4>
          <ol type = 1>
            <li>Coaching On One To One Ratio To Develop Your Cricket Skills To Greater Extent And To Gain Quick Results.</li>
            <li>Indoor Nets (Day Or Night As Per Players Choice)</li>
            <li>Coaching From Expert Coaches </li>
            <li>All The Drills Are Taught  One By One With 1000’s Of Repetition Action.</li>
            <li>Fitness Tips</li>
            <li>Video Analysis</li>
            <li>Real Match Situation</li>
            <li>Food And Accommodation For Outstation Players</li>
            <li>A Coaching Session Of 5 Days To 15 Days Is Provided To The Interested Players On One To One Basis .</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</section>
@include('partials.social-media')
@include('partials.javascripts')
@stop
