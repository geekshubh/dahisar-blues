@extends('layouts.app')
@section('content')
<section>
  <div class="container">
    <h2>Our Team</h2>
    <br><br>
    <div class="row">
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Deepak-Thakur.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Deepak Thakur</h5>
              <p class="card-text">President</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Ramesh-S-Singh.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Ramesh S Singh</h5>
              <p class="card-text">Vice President</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Deepak-Ghone.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Deepak Ghone</h5>
              <p class="card-text">Vice President</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Viral-Deepak-Thakur.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mrs. Viral Deepak Thakur</h5>
              <p class="card-text">Secretary</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Sapna-Thakur.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mrs. Sapna R Thakur</h5>
              <p class="card-text">Jt. Secretary</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4 mt-5">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Rajendra-Thakur.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Rajendra Thakur</h5>
              <p class="card-text">Jt. Secretary</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4 mt-5">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Mirant-Joshi.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Mirant Joshi</h5>
              <p class="card-text">Treasurer</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4 mt-5">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/stock-man.jpg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Praveen Wadekar</h5>
              <p class="card-text">Committee Member</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4 mt-5">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Jagdish-Oza.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Jagdish Oza</h5>
              <p class="card-text">Committee Member</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4 mt-5">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Sameer-Shinde.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Sameer Shinde</h5>
              <p class="card-text">Committee Member</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4 mt-5">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Elton-Sandhya-Ambrose.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Elton Ambrose</h5>
              <p class="card-text">Committee Member</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-4 mt-5">
        <div class="about_text">
          <div class="card" style="width: 18rem;">
            <img src="{{ url('img/team/Elton-Sandhya-Ambrose.jpeg')}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Mr. Sandhya Elton Ambrose</h5>
              <p class="card-text">Committee Member</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('partials.social-media')
@include('partials.javascripts')
@stop
