@extends('layouts.app')
@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xl-12">
        <div class="about_text">
          <h4>About us</h4>
          <h2>Welcome to Dahisar Blues Cricket Club
          </h2>
          <p style="font-color:black !important;">Dahisar Blues Cricket Club and Cultural Centre is established in the year 1997 by its founder Mr. Deepak.S.Thakur who is not only a sports passionate person but also an educationist.He was poor but always wanted to play leather ball cricket. he was fascinated by the great cricketers of India like of Sunil Gavaskar , Dilip Vengsarkar, Kapil Dev, Sandeep Patil, Ravi Shastri, Sayyad Kirmani, Kirti Azad, Mohindarsingh Amarnath etc., but it was not possible for him to pay for the coaching fees and travel without tickets to Dadar Shivaji Park and Churchate Azad maidan, as cricket was mainly played and coaching were provided in town (South Mumbai) so tennis cricket was the only option to fulfill the lust of playing season cricket in the year 1989, two youngsters from a middle class family arrived in the mumbai circuit  and created a world record of highest run partnership in school cricket,the great Sachin Tendulkar and Vinod Kambli has arrived on the state level and soon they went on to play for national side. The arrival of Sachin Tendulkar gripped the entire mumbai and the remote villages situated near mumbai into cricket fever, watching him playing on tv screen was such an amusement !
          </p>
          <br>
          <p>In the year 1995, a well known cricket club from suburban area "Dahisar Sports Club" started free coaching for all the cricket lovers who wants to excel in the cricket field and want to play season cricket. What an opportunity ! My happiness had no bounds after joining this club and touching the leather ball firt time in my life at the age of 18 years. I played for this club for 2 years and then thought that I must have to open a club for the poor and poverty ridden players of my area. There are so many players who wantd to play but was not able to join the club due to excess of players in dahisar sports club . so ultimately I have decided to open my club for them under the guidance of Mr Praveen Gogri Secretary of Dahisar Sports Club. He is such a kind person that he helped me with kit bags, nets and other sports equipments.Thus on 13th December 1997 Dahisar Blues Cricket was formed and practice started with 12-15 slum boys under my coachiing on a muncipal school ground at dahiar. The journey began from here …… It's still on.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<br><br>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-xl-6">
        <div class="about_text">
          <h2>Our Management Team</h2>
          <br>
          <ol type = 1>
            <li>Deepak S Thakur (President)</li>
            <li>Dayand Shetty (Vice President)</li>
            <li>Deepak Ghone  (Vice President)</li>
            <li>Mrs Viral D Thakur (Secretary)</li>
            <li>Mrs Sapna R Thakur (Jt. Secretary)</li>
            <li>Mirant Joshi(Treasurer)</li>
            <li>Praveen Wadekar (Committee Member)</li>
            <li>Jagdish Oza (Committee Member)</li>
            <li>Sameer Shinde (Committee Member)</li>
            <li>Elton Ambrose (Committee Member)</li>
            <li>Riyaz Siddiqui (Committee Member)</li>
            <li>Mrs Sandhya Ambrose (Committee Member)</li>
          </ol>
          <br>
        </div>
      </div>
      <div class="col-md-12 col-xl-6">
        <div class="about_text">
          <h2>DEEPAK THAKUR:</h2>
          <br>
          <p>Deepak Thakur is a teacher by profession. He is teaching maths and science for  the school section upto grade 10.He has worked as a maths teacher in Vibgyor High School, Billabong High International School, Sundaram Central High School, St. Xaviers High School etc. He has done his B.A, M.A, B.Ed, B.P.Ed. Mr Thakur is a sports enthusiast so he has attended o level cricket coaching organised by Mumbai Cricket Association. Mr Deepak Thakur is playing MCA official tournaments like Dr.H.D Kanga League, Bombay Junior, Cosmopolitan Shield, Salarjung, Knock Out, Purushottam Shield etc., from Marwari Cricket Club afilliated to MCA.Cricket coaching is a passion for Mr Deepak Thakur. Even today he gives cricket coaching to players of the club. He provides free cricket coaching to students of municipal school, poor children and all the girls who wish to play cricket.</p>
          <br>
        </div>
      </div>
    </div>
  </div>
</section>
@include('partials.social-media')
@include('partials.javascripts')
@stop
