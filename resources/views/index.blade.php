@extends('layouts.app')
@section('content')
<section class="banner_part">
  <div class="container">
    <div class="row align-content-center">
      <div class="col-lg-7 col-xl-5">
        <div class="banner_text">
          <h1><span>Dahisar Blues Cricket Club</span><br></h1>
          <a href="{{ route('our-offerings')}}" class="btn_1">Learn more <span class="ti-angle-right"></span> </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- banner part start-->
<section class="about_part">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-6 offset-xl-1 col-xl-4">
        <div class="about_img">
          <img src="{{ url('img/cricket-tour-december.jpeg')}}" alt="">
        </div>
      </div>
      <div class="col-md-6 col-xl-4">
        <div class="about_text">
          <h2>Upcoming Matches</h2>
          <p><b>Good news... Good news.. good news...!! Cricket tour to SURAT, GUJRAT. Dahisar Blues Cricket club has organised a 4days Cricket tour to Surat Gujarat from 23rd December to 26 th December 2019. Tour fixture. 23rd December Departure to Surat from Mumbai.</b>
            <br>
            1) 23/12/19 - 1st T20 match @ 2pm.
            <br>
            2) 24/12/19 - 40 overs match.
            <br>
            3) 25/12/19 - 9am T20 match & 2pm T20 match
            <br>
            5) 26/11/19 - 40 overs match
            <br>
            Departure to Mumbai in the evening at 6pm and 11pm Arrival at Mumbai.
          </p>
          <a href="{{ route('contact-us')}}" class="btn_2">Register</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- about part start-->
<section class="about_part">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-6 offset-xl-1 col-xl-4">
        <div class="about_img">
          <img src="img/second-pic-home-page.jpg" alt="">
        </div>
      </div>
      <div class="col-md-6 col-xl-4">
        <div class="about_text">
          <h4>About us</h4>
          <h2>Welcome <br>
            to Dahisar Blues Cricket Club
          </h2>
          <p>Dahisar Blues Cricket Club is established in the year 1995 by Mr. Deepak Thakur, a sports passionate person and a teacher by profession, the main aim of establishing the club is to provide cricketing facilities to the underprivileged children of local area on absolutely free of cost basis</p>
          <a href="{{ route('about')}}" class="btn_2">read more</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- about part start-->
<!-- upcoming_event part start-->
<section class="upcoming_event section_padding">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-5">
        <div class="section_tittle text-center">
          <h4>Batches</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-xl-6">
        <div class="upcoming_event_1">
          <img src="{{ url('img/batch_1_resized.jpg')}}" alt="#">
          <div class="upcoming_event_text">
            <div class="time">
              <ul class="list-unstyle">
                <li> <span class="ti-time"></span> 6:30 AM - 8:30 AM</li>
              </ul>
            </div>
            <p>Morning Batch</p>
            <a href="{{ route('contact-us')}}" class="btn_2">Register</a>
          </div>
        </div>
      </div>
      <div class="col-md-6  col-xl-6">
        <div class="upcoming_event_1">
          <img src="{{ url('img/homepage-3.jpeg')}}" alt="#">
          <div class="upcoming_event_text">
            <div class="time">
              <ul class="list-unstyle">
                <li> <span class="ti-time"></span> 4.30 PM - 6.30 PM</li>
              </ul>
            </div>
            <p>Evening Batch </p>
            <a href="{{ route('contact-us')}}" class="btn_2">Register</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="upcoming_event section_padding">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-5">
        <div class="section_tittle text-center">
          <h4>Batches</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-xl-6">
        <div class="upcoming_event_1">
          <img src="{{ url('img/batch_1_resized.jpg')}}" alt="#">
          <div class="upcoming_event_text">
            <div class="time">
              <ul class="list-unstyle">
                <li> <span class="ti-time"></span> 11:00 AM - 1:00 PM</li>
              </ul>
            </div>
            <p>Special Batch</p>
            <a href="{{ route('contact-us')}}" class="btn_2">Register</a>
          </div>
        </div>
      </div>
      <div class="col-md-6  col-xl-6">
        <div class="upcoming_event_1">
          <img src="{{ url('img/batch_2_2.jpg')}}" alt="#">
          <div class="upcoming_event_text">
            <div class="time">
              <ul class="list-unstyle">
                <li> <span class="ti-time"></span> 1.00 PM - 3.00 PM</li>
              </ul>
            </div>
            <p>Special Batch </p>
            <a href="{{ route('contact-us')}}" class="btn_2">Register</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- upcoming_event part start-->
<!-- learn_about part start-->
<section class="learn_about section_padding">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-5">
        <div class="section_tittle text-center">
          <h4>Few Video Tutorials</h4>
          <h2>Learn About our Academy</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12">
        <div class="swiper-container gallery-top">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="swiper-slide-container overlay">
                <img src="img/video_bg.jpg" alt="">
                <div class="slider_content">
                  <a id="play-video_1" class="video-play-button popup-youtube"  href="https://www.youtube.com/watch?v=0OCSRFBEcaw">
                  <span class="ti-control-play"></span>
                  </a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="swiper-slide-container overlay">
                <img src="img/second_video_bg.jpg" alt="">
                <div class="slider_content">
                  <a id="play-video_1" class="video-play-button popup-youtube"  href="https://www.youtube.com/watch?v=UklnA-0Paic">
                  <span class="ti-control-play"></span>
                  </a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="swiper-slide-container overlay">
                <img src="img/third_video_bg.jpg" alt="">
                <div class="slider_content">
                  <a id="play-video_1" class="video-play-button popup-youtube"  href="https://www.youtube.com/watch?v=RpFcPPN71Go">
                  <span class="ti-control-play"></span>
                  </a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="swiper-slide-container overlay">
                <img src="img/fourth_video_bg.jpg" alt="">
                <div class="slider_content">
                  <a id="play-video_1" class="video-play-button popup-youtube"  href="https://www.youtube.com/watch?v=agJmlPxqiko">
                  <span class="ti-control-play"></span>
                  </a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="swiper-slide-container overlay">
                <img src="img/fifth_video_bg.jpg" alt="">
                <div class="slider_content">
                  <a id="play-video_1" class="video-play-button popup-youtube"  href="https://www.youtube.com/watch?v=LeraKIn_NnY">
                  <span class="ti-control-play"></span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- Add Arrows -->
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('partials.social-media')
@include('partials.javascripts')
@stop
