<section class="footer-area section_padding">
  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-sm-4 mb-4 mb-xl-0 single-footer-widget">
        <h4>Contact Us</h4>
        <ul>
          <li>Mobile No. :- <a href="tel:+919920985112">+91 99209 85112 </a></li>
          <li>EMAIL ID :- <a href="mailto:dahisarbluescc167@gmail.com">dahisarbluescc167@gmail.com</a></li>
          <li>Address: N. L Complex Ground, Dahisar East, Mumbai, Maharashtra, India </li>
        </ul>
      </div>
      <div class="col-xl-3 col-sm-4 mb-4 mb-xl-0 single-footer-widget">
        <h4>Follow Us</h4>
        <ul>
          <li><a href="#">Facebook</a></li>
          <li><a href="#">Instagram</a></li>
          <li><a href="#">Twitter</a></li>
          <li><a href="#">LinkedIn</a></li>
          <li><a href="#">Whatsapp</a></li>
        </ul>
      </div>
      <div class="col-xl-3 col-sm-4 mb-4 mb-xl-0 single-footer-widget">
        <h4>Careers</h4>
        <p>Aspiring Cricket  Coaches (Experienced Or Fresher) Kindly Share Your Resume On Our Official Email ID</p>
      </div>

      <div class="col-xl-3 col-sm-8 col-md-8 mb-4 mb-xl-0 single-footer-widget">
          <h4>Donations / Sponsorship</h4>
        <p>Donations And Sponsorships Are Welcome. Kindly Make Payment In The Following Account. Dahisar Blues Cricket Club & Cultural Centre.</p>
      </div>
    </div>
  </div>
</section>
<footer class="copyright_part">
  <div class="container">
    <div class="row align-items-center">
      <p class="footer-text m-0 col-lg-8 col-md-12">
        Copyright &copy; 2019 All rights reserved </a>
      <div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
        <a href="#"><i class="ti-facebook"></i></a>
      </div>
    </div>
  </div>
</footer>
