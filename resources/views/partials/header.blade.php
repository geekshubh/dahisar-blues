<header class="header_area">
  <div class="sub_header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4 col-xl-6">
          <div id="logo">
            <a href="{{ route('index') }}"><img src="{{ url('img/logo-1.png')}}" alt="" title="" /></a>
          </div>
        </div>
        <div class="col-md-8 col-xl-6">
          <div class="sub_header_social_icon float-right">
            <a href="tel:+919920985112"><i class="flaticon-phone"></i>9920985112</a>
            <a href="{{ route('contact-us')}}" class="register_icon"><i class="ti-arrow-right"></i>REGISTER</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main_menu">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link active" href="{{ route('index')}}">Home</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('about')}}" class="nav-link">About us</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('team')}}" class="nav-link">Team</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('our-offerings')}}" class="nav-link">Our Offerings</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('net-practice')}}" class="nav-link">Net Practice</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('gallery')}}" class="nav-link">Gallery</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('matches')}}" class="nav-link">Matches</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('contact-us')}}" class="nav-link">Contact</a>
                </li>
              </ul>
              <div class="header_social_icon d-none d-lg-block">
                <ul>
                  <li><a href="#"><i class="ti-facebook"></i></a></li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>
