<script src="{{ url('js/jquery-1.12.1.min.js')}}"></script>
<!-- popper js -->
<script src="{{ url('js/popper.min.js')}}"></script>
<!-- bootstrap js -->
<script src="{{ url('js/bootstrap.min.js')}}"></script>
<!-- aos js -->
<script src="{{ url('js/aos.js')}}"></script>
<!-- easing js -->
<script src="{{ url('js/jquery.magnific-popup.js')}}"></script>
<!-- swiper js -->
<script src="{{ url('js/swiper.min.js')}}"></script>
<!-- swiper js -->
<script src="{{ url('js/masonry.pkgd.js')}}"></script>
<!-- particles js -->
<script src="{{ url('js/owl.carousel.min.js')}}"></script>
<!-- carousel js -->
<script src="{{ url('js/swiper.min.js')}}"></script>
<!-- swiper js -->
<script src="{{ url('js/swiper_custom.js')}}"></script>
<!-- custom js -->
<script src="{{ url('js/custom.js')}}"></script>
