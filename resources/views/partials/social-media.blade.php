<section class="social_connect_part">
  <div class="container-fluid">
    <div class="row justify-content-center ">
      <div class="col-xl-5">
        <div class="section_tittle text-center">
          <h4>Social Media</h4>
          <h2> Follow Us Instagram</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12">
        <div class="social_connect">
          <div class="single-social_connect">
            <div class="social_connect_img">
              <img src="img/instagram_1.jpg" class="" alt="blog">
              <div class="social_connect_overlay">
                <a href="#"><span class="ti-instagram"></span></a>
              </div>
            </div>
          </div>
          <div class="single-social_connect">
            <div class="social_connect_img">
              <img src="img/instagram_2.jpg" class="" alt="blog">
              <div class="social_connect_overlay">
                <a href="#"><span class="ti-instagram"></span></a>
              </div>
            </div>
          </div>
          <div class="single-social_connect">
            <div class="social_connect_img">
              <img src="img/instagram_3.jpg" class="" alt="blog">
              <div class="social_connect_overlay">
                <a href="#"><span class="ti-instagram"></span></a>
              </div>
            </div>
          </div>
          <div class="single-social_connect">
            <div class="social_connect_img">
              <img src="img/instagram_4.jpg" class="" alt="blog">
              <div class="social_connect_overlay">
                <a href="#"><span class="ti-instagram"></span></a>
              </div>
            </div>
          </div>
          <div class="single-social_connect">
            <div class="social_connect_img">
              <img src="img/instagram_5.jpg" class="" alt="blog">
              <div class="social_connect_overlay">
                <a href="#"><span class="ti-instagram"></span></a>
              </div>
            </div>
          </div>
          <div class="single-social_connect">
            <div class="social_connect_img">
              <img src="img/instagram_6.jpg" class="" alt="blog">
              <div class="social_connect_overlay">
                <a href="#"><span class="ti-instagram"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
