<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dahisar Blues Cricket Club</title>
<link rel="icon" href="{{ url('img/favicon.png')}}">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ url('css/bootstrap.min.css')}}">
<!-- animate CSS -->
<link rel="stylesheet" href="{{ url('css/animate.css')}}">
<!-- owl carousel CSS -->
<link rel="stylesheet" href="{{ url('css/owl.carousel.min.css')}}">
<!-- themify CSS -->
<link rel="stylesheet" href="{{ url('css/themify-icons.css')}}">
<!-- flaticon CSS -->
<link rel="stylesheet" href="{{ url('css/flaticon.css')}}">
<!-- font awesome CSS -->
<link rel="stylesheet" href="{{ url('css/magnific-popup.css')}}">
<!-- swiper CSS -->
<link rel="stylesheet" href="{{ url('css/swiper.min.css')}}">
<!-- style CSS -->
<link rel="stylesheet" href="{{ url('css/style.css')}}">
