@extends('layouts.app')
@section('content')
<div class="container">
  <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Gallery</h1>
  <hr class="mt-2 mb-5">
  <div class="row text-center text-lg-left">
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-1.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-2.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-3.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-4.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-5.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-6.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-7.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-8.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-9.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-10.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-11.jpg')}}" alt="">
      </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
      <img class="img-fluid img-thumbnail" src="{{ url('img/gallery/gallery-12.jpg')}}" alt="">
      </a>
    </div>
  </div>
</div>
<!-- /.container -->
@endsection
