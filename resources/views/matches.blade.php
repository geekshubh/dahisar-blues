@extends('layouts.app')
@section('content')
<section>
  <div class="container">
    <br><br>
    <div class="row">
      <div class="col-md-12 col-xl-12">
        <h2>Organise Matches with our team or register to play with us!</h2>
        <br><br><br>
        <div class="row">
          <div class="col-md-4 col-xl-4">
            <h2>Week Day Matches</h2>
            <p> There are matches orgainsed on Tuesday, Thursday and Friday by Dahisar Blues Cricket Club, to play against us or to be a part of the team, please contact us by clicking on the link below</p>
            <a class="btn btn-info btn-md" href="{{ route('contact-us')}}">Contact Us</a>
          </div>
          <div class="col-md-4 col-xl-4">
            <h2>Week End Matches</h2>
            <p> There are matches orgainsed on Saturday & Sunday by Dahisar Blues Cricket Club, to play against us or to be a part of the team, please contact us by clicking on the link below</p>
            <a class="btn btn-info btn-md" href="{{ route('contact-us')}}">Contact Us</a>
          </div>
          <div class="col-md-4 col-xl-4">
            <h2>Holiday Matches</h2>
            <p> There are matches orgainsed on Public Holidays by Dahisar Blues Cricket Club, to play against us or to be a part of the team, please contact us by clicking on the link below</p>
            <a class="btn btn-info btn-md" href="{{ route('contact-us')}}">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('partials.social-media')
@include('partials.javascripts')
@stop
