<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['uses'=>'WebsiteController@index','as'=>'index']);
Route::get('about-us',['uses'=>'WebsiteController@about','as'=>'about']);
Route::get('our-offerings',['uses'=>'WebsiteController@our_offering','as'=>'our-offerings']);
Route::get('team',['uses'=>'WebsiteController@team','as'=>'team']);
Route::get('contact-us',['uses'=>'WebsiteController@contact_us','as'=>'contact-us']);
Route::post('contact-us/send-email',['uses'=>'WebsiteController@send_email','as'=>'send-email']);
Route::get('gallery',['uses'=>'WebsiteController@gallery','as'=>'gallery']);
Route::get('net-practice',['uses'=>'WebsiteController@net_practice','as'=>'net-practice']);
Route::get('matches',['uses'=>'WebsiteController@matches','as'=>'matches']);
